﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;
using SoundSwapperShared;
using Newtonsoft.Json;

namespace SoundSwapper
{
    public class SoundSwapper : BaseScript
    {
        private readonly Dictionary<Model, SwapInfo> SoundRelationships = new Dictionary<Model, SwapInfo>();
        private readonly Dictionary<Vehicle, int> SwapWatcher = new Dictionary<Vehicle, int>();
        private Vehicle[] Traffic;

        private bool TrafficToo = false;
        private bool DebugMode = false;

        public SoundSwapper()
        {
            EventHandlers["onClientResourceStart"] += new Action<string>(OnClientResourceStart);
            EventHandlers["SoundSwapper:SendConfig"] += new Action<string>(HandleGetConfig);
        }

        private void OnClientResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            TriggerServerEvent("SoundSwapper:RequestConfig");
        }

        private void HandleGetConfig(string configstring)
        {
            Config config = JsonConvert.DeserializeObject<Config>(configstring);

            TrafficToo = config.ApplyToTraffic;
            foreach (var pair in config.SoundRelationships)
            {
                SoundRelationships.Add(new Model(pair.Key), pair.Value);
            }

            Tick += OnTick;
        }

        private async Task OnTick()
        {
            HandleSwaps();
            await Delay(500);
        }

        private int HandleTuningSwaps(Vehicle v, int lastMod)
        {
            if (CanWeUse(v) && SoundRelationships.ContainsKey(v.Model))
            {
                VehicleMod mod = v.Mods.GetAllMods().FirstOrDefault(modi => modi.ModType == (VehicleModType)SoundRelationships[v.Model].ModCategory);
                if (mod != null)
                {
                    int m = mod.Index;
                    if (m != lastMod)
                    {
                        if (SoundRelationships[v.Model].ModIDtoEngineSound.ContainsKey(m))
                        {
                            ForceVehicleEngineAudio(v.Handle, SoundRelationships[v.Model].ModIDtoEngineSound[m]);

                            if (DebugMode) Notify("~b~" + v.DisplayName + "~w~'s engine its swapped for a ~b~" + SoundRelationships[v.Model].ModIDtoEngineSound[m] + "~w~ engine.");
                        }

                        return m;
                    }
                }
            }
            return -2;
        }

        private int TrafficListCounter = 0;

        private void HandleSwaps()
        {
            foreach (Vehicle v in SwapWatcher.Keys)
            {
                if (CanWeUse(v))
                {
                    int n = HandleTuningSwaps(v, SwapWatcher[v]);
                    if (n > -2)
                    {
                        SwapWatcher[v] = n;
                        break;
                    }
                }
                else
                {
                    SwapWatcher.Remove(v);
                    break;
                }
            }

            Vehicle PlayerVeh = Game.Player.Character.CurrentVehicle;
            if (CanWeUse(PlayerVeh) && !SwapWatcher.ContainsKey(PlayerVeh) && SoundRelationships.ContainsKey(PlayerVeh.Model))
            {
                SwapWatcher.Add(PlayerVeh, -2);
            }


            if (TrafficToo)
            {
                if (Traffic == null)
                {
                    Traffic = World.GetAllVehicles();
                }
                else
                {
                    for (int i = TrafficListCounter; i < TrafficListCounter + 5; i++)
                    {
                        if (i > Traffic.Length - 1)
                        {
                            Traffic = null;
                            TrafficListCounter = 0;
                            break;
                        }
                        else
                        {
                            Vehicle veh = Traffic[TrafficListCounter];
                            if (CanWeUse(veh) && !SwapWatcher.ContainsKey(veh) && SoundRelationships.ContainsKey(veh.Model))
                            {
                                SwapWatcher.Add(veh, -2);
                            }

                            TrafficListCounter++;
                        }
                    }
                }
            }
        }

        private bool CanWeUse(Entity entity)
        {
            return entity != null && entity.Exists();
        }

        private static void Notify(string message, bool blink = true, bool saveToBrief = false)
        {
            SetNotificationTextEntry("CELL_EMAIL_BCON"); // 10x ~a~
            foreach (string s in CitizenFX.Core.UI.Screen.StringToArray(message))
            {
                AddTextComponentSubstringPlayerName(s);
            }
            DrawNotification(blink, saveToBrief);
        }

    }
}
