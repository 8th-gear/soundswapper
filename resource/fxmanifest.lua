fx_version 'cerulean'
games { 'gta5' }

author 'Noxis'
description 'Vehicle Sound Swapper based on Auto Engine Sound Swapper by Eddlm'
version '0.1.0'

files {
	"Newtonsoft.Json.dll"
}

client_script "SoundSwapper.net.dll"

server_script "SoundSwapperServer.net.dll"
