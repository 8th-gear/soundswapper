﻿using CitizenFX.Core;
using SoundSwapperShared;
using System;
using System.Collections.Generic;
using System.Xml;
using static CitizenFX.Core.Native.API;
using Newtonsoft.Json;

namespace SoundSwapperServer
{
    public class SoundSwapperServer : BaseScript
    {
        private Config config;

        public SoundSwapperServer()
        {
            LoadSettings();
            EventHandlers["SoundSwapper:RequestConfig"] += new Action<Player>(HandleSendConfig);
        }

        private void HandleSendConfig([FromSource]Player player)
        {
            player.TriggerEvent("SoundSwapper:SendConfig", JsonConvert.SerializeObject(config));
        }

        private void LoadSettings()
        {
            Debug.WriteLine("starting load");

            config = new Config()
            {
                SoundRelationships = new Dictionary<string, SwapInfo>()
            };

            string data = LoadResourceFile("SoundSwapper", "config.xml");

            XmlDocument file = new XmlDocument();
            file.LoadXml(data);

            config.ApplyToTraffic = bool.Parse(file.DocumentElement.GetAttribute("ApplyToTraffic"));

            foreach (XmlElement swapset in file.DocumentElement.ChildNodes)
            {
                SwapInfo s = new SwapInfo
                {
                    ModCategory = int.Parse(swapset.GetAttribute("ModType"))
                };
                foreach (XmlElement swap in swapset)
                {
                    if (swap.HasAttribute("EngineSound")) s.ModIDtoEngineSound.Add(int.Parse(swap.GetAttribute("ModID")), swap.GetAttribute("EngineSound"));
                }
                config.SoundRelationships.Add(swapset.GetAttribute("CarName"), s);
            }

            Debug.WriteLine("SoundSwap Setup Complete");
        }
    }
}
