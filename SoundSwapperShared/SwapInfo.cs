﻿using System.Collections.Generic;

namespace SoundSwapperShared
{
    public class SwapInfo
    {
        public int ModCategory;
        public Dictionary<int, string> ModIDtoEngineSound = new Dictionary<int, string>();
    }

    public class Config
    {
        public bool ApplyToTraffic;
        public Dictionary<string, SwapInfo> SoundRelationships;
    }
}
